// @flow

import React, { useContext, useRef } from 'react'
import { Point, KeyEvent, ToolEvent } from 'paper/dist/paper-core'
import { Tool } from 'react-paper-renderer'
import { Context } from '../context'
import { findSelectionItem, useTimeout } from '../utils'
import type { PaperItem } from '../item/types'
import Icon from '@material-ui/icons/TouchApp'
import Button from './ToolButton'

const NAME = 'select'

export function SelectButton() {
  return (
    <Button name={NAME}>
      <Icon />
    </Button>
  )
}

export function SelectTool() {
  const item = useRef<PaperItem|null>(null)
  const point = useRef<Point|null>(null)
  const changed = useRef<boolean>(false)
  const setTimeout = useTimeout()
  const { actions, activeTool, paper } = useContext(Context)

  function handleMouseDown(e: ToolEvent) {
    const scope = paper.getScope()
    if (!scope) return

    const hit = scope.project.hitTest(e.point, {
      fill: true,
      segments: true,
      stroke: true,
    })
    const hitItem = hit && hit.item

    // already selected
    if (hitItem && hitItem === item.current) {
      return
    }

    // select item
    const selItem = hit && hit.item && findSelectionItem(hit.item)
    if (selItem) {
      selItem.bringToFront()
      actions.route.set(selItem.layer.data.routeId)
      paper.selectItem(selItem.data.selectionId)
      item.current = selItem
      point.current = e.point
      return 
    }

    // deselect
    paper.selectItem(null)
    item.current = null
    point.current = null
  }

  /*
  function handleMouseDrag(e: ToolEvent) {
    if (item.current && point.current) {
      item.current.translate(e.point.subtract(point.current))
      changed.current = true
      point.current = e.point
    }
  }

  function handleMouseUp() {
    if (item.current && changed.current) {
      switch (item.current.type) {
        case 'Avatar':
        case 'Comment':
        case 'Icon':
        case 'Video':
          actions.item.update(item.current, {
            position: [item.current.position.x, item.current.position.y],
          })
          break
        case 'Circle':
        case 'Path':
          actions.item.update(item.current, {
            pathData: item.current.pathData,
          })
          break
        case 'Pitch':
          actions.item.update(item.current, {
            pathData: item.current.children['Path'].pathData,
          })
          break
      }
    }

    changed.current = false
    point.current = null
  }
  */

  function handleKeyDown(e: KeyEvent) {
    if (!item.current) return

    const {
      key,
      modifiers: { shift },
    } = e

    switch (key) {
      case 'delete':
        actions.item.remove(item.current)
        changed.current = false
        item.current = null
        point.current = null
        break
      case 'up':
        item.current.translate(0, shift ? -10 : -1)
        changed.current = true
        break
      case 'down':
        item.current.translate(0, shift ? 10 : 1)
        changed.current = true
        break
      case 'left':
        item.current.translate(shift ? -10 : -1, 0)
        changed.current = true
        break
      case 'right':
        item.current.translate(shift ? 10 : 1, 0)
        changed.current = true
        break
      default:
        break
    }
  }

  function handleKeyUp(e: KeyEvent) {
    if (!item.current || !changed.current || e.key === 'shift') return

    // debounce history update
    // when user preses some key multiple times
    // we don't immediately record history change
    // because we would end up with many small changes

    const { current } = item
    const { pathData } = current

    setTimeout(() => {
      actions.item.update(current, { pathData })
      changed.current = false
    }, 350)
  }

  return (
    <Tool
      active={activeTool === NAME}
      data={{ name: NAME }}
      onKeyUp={handleKeyUp}
      onKeyDown={handleKeyDown}
      onMouseDown={handleMouseDown}
      //onMouseDrag={handleMouseDrag}
      //onMouseUp={handleMouseUp}
    />
  )
}
