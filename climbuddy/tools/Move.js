// @flow

import React, { useContext, useEffect, useRef } from 'react'
import { ToolEvent } from 'paper/dist/paper-core'
import { Tool } from 'react-paper-renderer'
import { Context } from '../context'
import Icon from '@material-ui/icons/PanTool'
import Button from './ToolButton'
import { doubleTap } from '../utils'

const NAME = 'move'
const ZOOM_RATIO = 1.1

export function MoveButton() {
  return (
    <Button name={NAME}>
      <Icon />
    </Button>
  )
}

export function MoveTool() {
  const pinchRef = useRef(null)
  const panRef = useRef(null)
  const { actions, activeTool, image, paper } = useContext(Context)

  function handleMouseDown(e: ToolEvent) {
    const scope = paper.getScope()
    if (!scope || !image) return
    
    if (doubleTap(e.event)) {
      return actions.image.reset()
    }
    
    const hit = scope.project.hitTest(e.point, {
      fill: true,
      segments: true,
      stroke: true,
    })
    if (!hit || !hit.item) return
    
    actions.route.set(hit.item.layer.data.routeId)
  }

  function handleMouseDrag(e: ToolEvent) {
    if (!image) return

    e.preventDefault()
    e.stopPropagation()

    if (e.event instanceof TouchEvent && e.event.touches.length === 2) {
      const next = getPinchEventData(e.event)
      if (pinchRef.current) {
        const state = getPinchEventState(e, pinchRef.current, next)
        const { sx, sy, tx, ty, zoom } = state
        e.tool.view.scale(zoom, [sx, sy])
        e.tool.view.translate(tx, ty)
      }
      pinchRef.current = next
    } else {
      const next = getPanEventData(e)
      if (panRef.current) {
        const { tx, ty } = getPanEventState(e, panRef.current)
        e.tool.view.translate(tx, ty)
      }
      panRef.current = next
    }
  }

  function handleMouseUp(e: ToolEvent) {
    e.preventDefault()
    e.stopPropagation()

    panRef.current = null
    pinchRef.current = null
  }

  function handleMouseWheel(e: WheelEvent) {
    const scope = paper.getScope()
    if (!scope) return

    const { view } = scope

    // wheel target is not canvas
    if (view.element !== e.target) return

    // stop wheel event to prevent browser scroll
    e.preventDefault()
    e.stopPropagation()

    const { clientX, clientY } = e
    const { left, top } = view.element.getBoundingClientRect()

    // calculate new zoom from wheel event delta
    const newZoom = -e.deltaY > 0 ? 1 * ZOOM_RATIO : 1 / ZOOM_RATIO

    // convert mouse point to project space
    const center = view.viewToProject(clientX - left, clientY - top)

    // transform view
    view.scale(newZoom, center)
    actions.image.zoom(view.zoom)
  }

  useEffect(
    () => {
      if (activeTool === NAME) {
        document.addEventListener('wheel', handleMouseWheel)
      }
      return () => {
        if (activeTool === NAME) {
          document.removeEventListener('wheel', handleMouseWheel)
        }
      }
    },
    [activeTool]
  )

  return (
    <Tool
      active={activeTool === NAME}
      data={{ name: NAME }}
      onMouseDown={handleMouseDown}
      onMouseDrag={handleMouseDrag}
      onMouseUp={handleMouseUp}
    />
  )
}

// pan helpers

function getPanEventData(e: ToolEvent) {
  const {
    point,
    event,
    tool: { view },
  } = e
  if (event instanceof TouchEvent) {
    return {
      point: view.projectToView(point),
      x: event.touches[0].pageX,
      y: event.touches[0].pageY,
    }
  } else {
    return {
      point: view.projectToView(point),
      x: event.pageX,
      y: event.pageY,
    }
  }
}

function getPanEventState(e: ToolEvent, prev: Object) {
  const {
    point,
    tool: { view },
  } = e
  const t = point.subtract(view.viewToProject(prev.point))
  return {
    tx: t.x,
    ty: t.y,
    x: t.x,
    y: t.y,
  }
}

// pinch helpers

function getPinchEventData({ touches }: TouchEvent, box?: Object = {}) {
  const { top = 0, left = 0 } = box

  // touch points
  const x0 = touches[0].pageX - left
  const y0 = touches[0].pageY - top
  const x1 = touches[1].pageX - left
  const y1 = touches[1].pageY - top

  // center point between fingers
  const center = [(x0 + x1) / 2, (y0 + y1) / 2]

  // distance between fingers
  const distance = Math.sqrt(Math.pow(x1 - x0, 2) + Math.pow(y1 - y0, 2))

  // return object describing touch state
  return { center, distance }
}

function getPinchEventState(e: ToolEvent, prev: Object, next: Object) {
  const {
    tool: { view },
  } = e

  // convert next center to view project space
  const center = view.viewToProject(next.center)

  // calculate distance between next and previous center
  const t = center.subtract(view.viewToProject(prev.center))

  // calculate next transformation scale
  const scale = next.distance / prev.distance

  // return object describing the touch transformation
  return {
    tx: t.x,
    ty: t.y,
    sx: center.x,
    sy: center.y,
    x: t.x,
    y: t.y,
    zoom: scale,
  }
}
