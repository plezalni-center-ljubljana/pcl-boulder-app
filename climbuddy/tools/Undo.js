// @flow

import React, { useContext } from 'react'
import UndoIcon from '@material-ui/icons/Undo'
import { Context } from '../context'
import IconButton from './IconButton'

export function UndoButton() {
  const { dispatch, historyIndex } = useContext(Context)
  return (
    <IconButton
      disabled={historyIndex === 0}
      onClick={() => dispatch({ type: 'undo' })}
    >
      <UndoIcon />
    </IconButton>
  )
}
