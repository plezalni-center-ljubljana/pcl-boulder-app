// @flow

import React, { useContext } from 'react'
import RedoIcon from '@material-ui/icons/Redo'
import { Context } from '../context'
import IconButton from './IconButton'

export function RedoButton() {
  const { dispatch, history, historyIndex } = useContext(Context)
  return (
    <IconButton
      disabled={historyIndex >= history.length - 1}
      onClick={() => dispatch({ type: 'redo' })}
    >
      <RedoIcon />
    </IconButton>
  )
}
