// @flow

import React, { useContext, useRef } from 'react'
import { Path, ToolEvent } from 'paper/dist/paper-core'
import { Tool } from 'react-paper-renderer'
import { Context } from '../context'
import Icon from '@material-ui/icons/Edit'
import Button from './ToolButton'

const NAME = 'pen'

export function PenButton() {
  return (
    <Button name={NAME}>
      <Icon />
    </Button>
  )
}

export function PenTool() {
  const pathRef = useRef<Path|null>(null)
  const { actions, activeTool, paper } = useContext(Context)

  function handleMouseDown() {
    paper.selectItem(null)
  }

  function handleMouseDrag(e: ToolEvent) {
    const scope = paper.getScope()
    if (!scope) return

    if (!pathRef.current) {
      pathRef.current = new scope.Path({
        segments: [e.point],
        selected: true,
        strokeColor: 'red',
        strokeScaling: false,
        strokeWidth: 2,
      })
    } else {
      pathRef.current.add(e.point)
    }
  }

  function handleMouseUp() {
    const { current: path } = pathRef
    if (path) {
      path.simplify(6)

      actions.item.add({
        type: 'Path',
        pathData: path.pathData,
        strokeColor: 'red',
        strokeScaling: false,
        strokeWidth: 2,
      })

      path.remove()
      pathRef.current = null
    }
  }

  return (
    <Tool
      active={activeTool === NAME}
      data={{ name: NAME }}
      onMouseDown={handleMouseDown}
      onMouseDrag={handleMouseDrag}
      onMouseUp={handleMouseUp}
    />
  )
}
