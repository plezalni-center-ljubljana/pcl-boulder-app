// @flow

import React, { useContext } from 'react'
import type { Node } from 'react'
import classnames from 'classnames'
import { makeStyles } from '@material-ui/styles'
import { Context } from '../context'
import IconButton from './IconButton'

const useStyles = makeStyles({
  root: {},
  active: {
    color: 'orange',
  },
})

type Props = {
  children: Node,
  className?: string,
  name: string,
}

const ToolButton = ({ children, className, name, ...other }: Props) => {
  const { activeTool, dispatch } = useContext(Context)
  const classes = useStyles()

  function handleClick() {
    if (activeTool !== name) {
      dispatch({ type: 'setActiveTool', tool: name })
    }
  }

  return (
    <IconButton
      {...other}
      className={classnames(
        classes.root,
        {
          [classes.active]: activeTool === name,
        },
        className
      )}
      onClick={handleClick}
    >
      {children}
    </IconButton>
  )
}

export default ToolButton
