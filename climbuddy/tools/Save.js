// @flow

import React, { useContext } from 'react'
import SaveIcon from '@material-ui/icons/Save'
import { Context } from '../context'
import IconButton from './IconButton'

export function SaveButton() {
  const { historyIndex, paper } = useContext(Context)
  return (
    <IconButton
      disabled={historyIndex > 0}
      onClick={paper.exportSvg}
    >
      <SaveIcon />
    </IconButton>
  )
}
