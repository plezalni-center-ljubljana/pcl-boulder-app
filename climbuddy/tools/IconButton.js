// @flow

import React from 'react'
import classnames from 'classnames'
import IconButton from '@material-ui/core/IconButton'
import { makeStyles } from '@material-ui/styles'
import type { Node } from 'react'

const useStyles = makeStyles({
  root: {
    background: 'rgba(255,255,255,0.3)',
  },
})

type Props = {
  children: Node,
  className?: string,
}

const ToolIconButton = ({ children, className, ...other }: Props) => {
  const classes = useStyles()
  return (
    <IconButton {...other} className={classnames(classes.root, className)}>
      {children}
    </IconButton>
  )
}

export default ToolIconButton
