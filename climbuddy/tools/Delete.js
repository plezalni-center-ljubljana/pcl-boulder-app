// @flow

import React, { useContext } from 'react'
import { ToolEvent } from 'paper/dist/paper-core'
import { Tool } from 'react-paper-renderer'
import { Context } from '../context'
import Icon from '@material-ui/icons/Delete'
import Button from './ToolButton'
import { findSelectionItem } from '../utils'

const NAME = 'delete'

export function DeleteButton() {
  return (
    <Button name={NAME}>
      <Icon />
    </Button>
  )
}

export function DeleteTool() {
  const { actions, activeTool, paper, route } = useContext(Context)

  function handleMouseDown(e: ToolEvent) {
    const scope = paper.getScope()
    if (!scope) return

    const hit = scope.project.hitTest(e.point, {
      fill: true,
      segments: true,
      stroke: true,
    })
    if (!hit || !hit.item) return

    const { routeId } = hit.item.layer.data
    if (!route || routeId !== route.id) return

    const selItem = findSelectionItem(hit.item)
    if (selItem) {
      actions.item.remove(selItem)
    }
  }

  return (
    <Tool
      active={activeTool === NAME}
      data={{ name: NAME }}
      onMouseDown={handleMouseDown}
    />
  )
}
