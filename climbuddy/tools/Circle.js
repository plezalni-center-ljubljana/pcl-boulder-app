// @flow

import React, { useContext, useRef } from 'react'
import type { Circle } from 'paper/dist/paper-core'
import { ToolEvent } from 'paper/dist/paper-core'
import { Tool } from 'react-paper-renderer'
import { Context } from '../context'
import Icon from '@material-ui/icons/RadioButtonUnchecked'
import Button from './ToolButton'

const NAME = 'circle'

export function CircleButton() {
  return (
    <Button name={NAME}>
      <Icon />
    </Button>
  )
}

const pathProps = {
  strokeColor: '#ff0000',
  strokeWidth: 2,
  strokeScaling: false,
}

export function CircleTool() {
  const { actions, activeTool, paper } = useContext(Context)
  const circleRef = useRef<Circle|null>(null)

  function handleMouseDown(e: ToolEvent) {
    const scope = paper.getScope()
    if (!scope) return

    circleRef.current = new scope.Path.Circle({
      ...pathProps,
      center: e.point,
      radius: 10,
    })
  }

  function handleMouseDrag(e: ToolEvent) {
    const scope = paper.getScope()
    const circle = circleRef.current
    if (!scope || !circle) return
    
    const center = e.downPoint
    const radius = center.getDistance(e.point)
    if (radius < 1) return

    const x = center.x - radius
    const y = center.y - radius
    const size = radius * 2

    circle.fitBounds(x, y, size, size)
  }

  function handleMouseUp() {
    if (!circleRef.current) return
    const circle = circleRef.current

    actions.item.add({
      ...pathProps,
      type: 'Circle',
      pathData: circle.pathData,
    })

    circle.remove()
    circleRef.current = null
  }

  return (
    <Tool
      active={activeTool === NAME}
      data={{ name: NAME }}
      onMouseDown={handleMouseDown}
      onMouseDrag={handleMouseDrag}
      onMouseUp={handleMouseUp}
    />
  )
}
