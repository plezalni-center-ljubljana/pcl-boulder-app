// @flow

import React, { useContext } from 'react'
import Dialog from '@material-ui/core/Dialog'
import Zoom from '@material-ui/core/Zoom'
import { Context } from '../context'

const PaperDialog = () => {
  const { actions, popup } = useContext(Context)

  return (
    <Dialog
      {...popup.props}
      open={popup.open}
      onClose={actions.popup.close}
      TransitionComponent={Zoom}
    >
      {popup.component || ''}
    </Dialog>
  )
}

export default PaperDialog
