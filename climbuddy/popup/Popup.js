// @flow

import React, { useContext } from 'react'
import type { Node } from 'react'

import CloseIcon from '@material-ui/icons/Close'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import IconButton from '@material-ui/core/IconButton'
import { makeStyles } from '@material-ui/styles'

import { Context } from '../context'

const useStyles = makeStyles({
  title: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 8,
  },
  icon: {
    width: 48,
    height: 48,
  },
  avatar: {
    width: 56,
    height: 56,
    float: 'left',
    marginRight: 16,
  },
  dialog: {
    paddingBottom: 24,
  }
})

type Props = {
  children: Node,
  title: string,
}

const PaperPopup = ({ children, title }: Props) => {
  const { actions } = useContext(Context)
  const classes = useStyles()

  return (
    <>
      <div className={classes.title}>
        <DialogTitle>{title}</DialogTitle>
        <IconButton className={classes.icon} onClick={actions.popup.close}>
          <CloseIcon />
        </IconButton>
      </div>
      <DialogContent className={classes.dialog}>{children}</DialogContent>
    </>
  )
}

export default PaperPopup
