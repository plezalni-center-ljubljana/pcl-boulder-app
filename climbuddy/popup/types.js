// @flow

import type { Node } from 'react'

export type PopupActions = {
  open(component: Node, props?: ?Object): void,
  close(): void,
}

export type Origin = {
  x: number,
  y: number,
}
