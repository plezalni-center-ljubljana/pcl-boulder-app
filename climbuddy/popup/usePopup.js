// @flow

import type { Node } from 'react'
import type { StateArray } from '../context/types'
import type { PopupActions, Origin } from './types'
import { emptyFunction, useTimeout } from '../utils'

export const popupActions = {
  open: emptyFunction,
  close: emptyFunction,
}

export function usePopup([,dispatch]: StateArray): PopupActions {
  const setTimeout = useTimeout()

  function open(component: Node, origin?: ?Origin = null) {
    dispatch({
      type: 'openPopup',
      component,
      props: getProps(origin),
    })
  }

  function close() {
    dispatch({ type: 'hidePopup' })
    setTimeout(() => {
      dispatch({ type: 'removePopup' })
    }, 300)
  }

  return {
    open,
    close,
  }
}

function getProps(origin?: ?Origin) {
  return Object.assign(
    {},
    {
      fullScreen: typeof window !== 'undefined' && window.innerWidth < 600,
    },
    origin && {
      TransitionProps: {
        style: {
          transformOrigin: `${origin.x}px ${origin.y}px`,
        },
      },
    }
  )
}
