// @flow

import { createContext } from 'react'
import { emptyFunction } from '../utils'

import { useDrawer, drawerActions } from '../drawer'
import { useImage, imageActions } from '../image'
import { useItem, itemActions } from '../item'
import { usePopup, popupActions } from '../popup'
import { useRoute, routeActions } from '../route'
import { usePaper, paperActions } from '../paper'
import { useState, defaultState } from './state'

import type { $Context, InitialState } from './types'

const Context = createContext<$Context>({
  ...defaultState,
  dispatch: emptyFunction,
  paper: paperActions,
  actions: {
    drawer: drawerActions,
    image: imageActions,
    item: itemActions,
    popup: popupActions,
    route: routeActions,
  },
})

export function usePaperContext(initialState?: InitialState): $Context {
  const $state = useState(initialState)
  const paper = usePaper($state)
  const image = useImage($state, paper)
  const route = useRoute($state)
  const item = useItem($state)
  const drawer = useDrawer($state)
  const popup = usePopup($state)
  
  const [state, dispatch] = $state

  return {
    ...state,
    paper,
    dispatch,
    actions: {
      image,
      route,
      item,
      drawer,
      popup,
    },
  }
}

const { Consumer, Provider } = Context

export { Context, Provider, Consumer }
