// @flow

import type { BaseState, Action } from './types'
import update from 'immutability-helper'

export function reducer(state: BaseState, action: Action): BaseState {
  //console.log(action)
  switch (action.type) {
    case 'setEditing': {
      return update(state, { $merge: {
        editing: action.editing,
        activeTool: !action.editing ? 'move' : state.activeTool,
      }})
    }
    case 'setImage': {
      const { image } = action
      const routes = image ? image.routes : []
      const route = routes.length ? routes[0] : null
      return update(state, { $merge: {
        image,
        route,
        routeIndex: route ? 0 : -1,
        history: image ? [image] : [],
        selection: route ? route.key : null,
      }})
    }
    case 'setRoute': {
      const image = state.history[state.historyIndex]
      return update(state, { $merge: {
        route: action.route,
        routeIndex: image.routes.indexOf(action.route),
      }})
    }
    case 'setActiveTool': {
      return update(state, { activeTool: { $set: action.tool } })
    }
    case 'setExpanded': {
      console.log('setttttttttttt')
      return update(state, { expanded: { $set: action.expanded } })
    }
    case 'setSelection': {
      return update(state, { selection: { $set: action.item } })
    }
    case 'setZoom': {
      return update(state, { zoom: { $set: action.zoom } })
    }
    case 'setDrawer': {
      return update(state, { drawer: { $merge: action.drawer } })
    }
    case 'openDrawer': {
      return update(state, {
        drawer: {
          [action.variant]: { $set: true },
        },
      })
    }
    case 'closeDrawer': {
      return update(state, {
        drawer: {
          [action.variant]: { $set: false },
        },
      })
    }
    case 'toggleDrawer': {
      return update(state, {
        drawer: {
          [action.variant]: { $set: !state.drawer[action.variant] },
        },
      })
    }
    case 'openPopup': {
      return update(state, {
        popup: { $set: {
          open: true,
          component: action.component,
          props: action.props,
        }},
      })
    }
    case 'hidePopup': {
      return update(state, {
        popup: { $merge: {
          open: false,
        }},
      })
    }
    case 'removePopup': {
      return update(state, {
        popup: { $merge: {
          component: null,
          props: null,
        }},
      })
    }
    case 'addHistory': {
      const historyIndex = state.historyIndex + 1
      const history = [
        ...state.history.slice(0, historyIndex),
        action.history,
      ]
      return update(state, { $merge: {
        history,
        historyIndex,
      }})
    }
    case 'undo': {
      if (state.historyIndex <= 0) {
        return state
      }
      return update(state, { $merge: {
        historyIndex: state.historyIndex - 1,
        selection: null,
      }})
    }
    case 'redo': {
      if (state.historyIndex >= state.history.length - 1) {
        return state
      }
      return update(state, { $merge: {
        historyIndex: state.historyIndex + 1,
        selection: null,
      }})
    }
    case 'reset': {
      return update(state, { $merge: {
        history: [state.image],
        historyIndex: 0,
        selection: null,
      }})
    }
    default: {
      return state
    }
  }
}