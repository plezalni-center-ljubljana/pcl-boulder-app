// @flow

import type { Node } from 'react'
import type { DrawerActions } from '../drawer/types'
import type { ImageActions, Image } from '../image/types'
import type { ItemActions } from '../item/types'
import type { PaperActions } from '../paper/types'
import type { PopupActions } from '../popup/types'
import type { RouteActions, Route } from '../route/types'

export type DrawerState = {
  desktop: boolean,
  mobile: boolean,
}

export type PopupState = {
  open: boolean,
  component: ?Node,
  props: ?Object,
}

export type BaseState = {
  activeTool: string,
  drawer: DrawerState,
  editing: boolean,
  expanded: Object,
  history: History,
  historyIndex: number,
  image: ?Image,
  popup: PopupState,
  route: ?Route,
  routeIndex: number,
  selection: ?string,
  zoom: number,
}

export type InitialState = {
  activeTool?: string,
  drawer?: DrawerState,
  editing?: boolean,
  expanded?: Object,
  history?: History,
  historyIndex?: number,
  image?: ?Image,
  popup?: PopupState,
  route?: ?Route,
  routeIndex?: number,
  selection?: ?string,
  zoom?: number,
}

export type State = BaseState

export type History = Array<HistoryItem>
export type HistoryItem = Image

declare function dispatch(action: Action): void
export type Dispatch = typeof dispatch

export type StateArray = [State, Dispatch]

export type ContextActions = {
  drawer: DrawerActions,
  image: ImageActions,
  item: ItemActions,
  popup: PopupActions,
  route: RouteActions,
}

export type $Context = State & {
  actions: ContextActions,
  dispatch: Dispatch,
  paper: PaperActions,
}


type SetEditingAction = {
  type: 'setEditing',
  editing: boolean,
}

type SetImageAction = {
  type: 'setImage',
  image: ?Image,
}
type SetRouteAction = {
  type: 'setRoute',
  route: ?Route
}
type EditRouteAction = {
  type: 'editRoute',
  route: Route
}
type SetActiveToolAction = {
  type: 'setActiveTool',
  tool: string,
}
type SetExpandedAction = {
  type: 'setExpanded',
  expanded: boolean,
}
type SetSelectionAction = {
  type: 'setSelection',
  item: ?string,
}
type SetZoomAction = {
  type: 'setZoom',
  zoom: number,
}
type SetDrawerAction = {
  type: 'setDrawer',
  drawer: Object,
}
type OpenDrawerAction = {
  type: 'openDrawer',
  variant: string,
}
type CloseDrawerAction = {
  type: 'closeDrawer',
  variant: string,
}
type ToggleDrawerAction = {
  type: 'toggleDrawer',
  variant: string,
}
type OpenPopupAction = {
  type: 'openPopup',
  component: any,
  props: Object,
}
type HidePopupAction = {
  type: 'hidePopup',
}
type RemovePopupAction = {
  type: 'removePopup',
}
type AddHistoryAction = {
  type: 'addHistory',
  history: HistoryItem,
}
type UndoAction = {
  type: 'undo',
}
type RedoAction = {
  type: 'redo',
}
type ResetAction = {
  type: 'reset',
}

export type Action = 
  | SetEditingAction
  | SetImageAction
  | SetRouteAction
  | EditRouteAction
  | SetActiveToolAction
  | SetExpandedAction
  | SetSelectionAction
  | SetZoomAction
  | SetDrawerAction
  | OpenDrawerAction
  | CloseDrawerAction
  | ToggleDrawerAction
  | OpenPopupAction
  | HidePopupAction
  | RemovePopupAction
  | AddHistoryAction
  | UndoAction
  | RedoAction
  | ResetAction