// @flow

import { useReducer } from 'react'
import { reducer } from './reducer'
import type { BaseState, InitialState, StateArray, Action } from './types'

export const defaultState: BaseState = {
  activeTool: 'move',
  drawer: {
    desktop: true,
    mobile: false,
  },
  editing: true,
  expanded: {},
  history: [],
  historyIndex: 0,
  image: null,
  popup: {
    open: false,
    component: null,
    props: null,
  },
  route: null,
  routeIndex: -1,
  selection: null,
  zoom: 1,
}

export function useState(inputState?: InitialState): StateArray {
  const initialState = { ...defaultState, ...inputState }
  
  const [base, dispatch] = useReducer<BaseState, Action>(reducer, initialState)
  
  // use image and route from the history
  const image = base.history[base.historyIndex]
  const route = image && base.route ? image.routes[base.routeIndex] : null

  return [{ ...base, image, route }, dispatch]
}

