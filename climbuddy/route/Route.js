// @flow

import React, { useContext } from 'react'
import { Layer } from 'react-paper-renderer'
import { Context } from '../context'
import { Items } from '../item'
import type { Route } from './types'

type Props = {
  route: Route,
}

export default function PaperRoute({ route }: Props) {
  const ctx = useContext(Context)
  const { activeTool, image, route: currentRoute, selection } = ctx
  const active = !!currentRoute && route.id === currentRoute.id

  return (
    <Layer
      active={active}
      data={{
        routeId: route.id,
        selectionId: route.key,
      }}
      visible={!!image}
    >
      {route.items.map(({ id, key, type, ...other }) => {
        const Item = Items[type]
        if (typeof Item === 'undefined' || !Item) {
          throw new Error(`Invalid item type ${type} in Route!`)
        }
        return (
          // $FlowFixMe
          <Item
            {...other}
            key={id}
            active={active}
            data={{
              itemId: id,
              selectionId: key,
            }}
            selected={
              activeTool === 'select' &&
              !!selection &&
              (selection === route.key || selection === key)
            }
          />
        )
      })}
    </Layer>
  )
}
