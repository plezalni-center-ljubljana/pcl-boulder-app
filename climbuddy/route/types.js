// @flow

import type { Item } from '../item/types'

export type Route = {
  id: number,
  key: string,
  name: string,
  items: Array<Item>,
}

export type RouteInput = {
  id?: number,
  key?: string,
  name?: string,
  items?: Array<Item>,
}

export type RouteOrID = Route | number | string

export type RouteActions = {
  set(route: ?RouteOrID): void,
  expand(route: ?RouteOrID): void,
  collapse(route: ?RouteOrID): void,
  toggle(route: ?RouteOrID): void,
  add(data: Object): void,
  edit(route: Route): void,
  update(route: Route, data: Object): void,
  remove(route: Route): void,
}
