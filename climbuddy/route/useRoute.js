// @flow

import _update from 'immutability-helper'
import { emptyFunction } from '../utils'
import type { StateArray, State } from '../context/types'
import type { Image } from '../image/types'
import type { Route, RouteOrID, RouteActions } from './types'

export const routeActions = {
  set: emptyFunction,
  expand: emptyFunction,
  collapse: emptyFunction,
  toggle: emptyFunction,
  add: emptyFunction,
  edit: emptyFunction,
  update: emptyFunction,
  remove: emptyFunction,
}

export function useRoute([state, dispatch]: StateArray): RouteActions {
  function set(route: ?RouteOrID) {
    route = getRoute(state, route)
    if (route === state.route) return
    dispatch({ type: 'setRoute', route })
  }

  function expand(route: ?RouteOrID) {
    route = getRoute(state, route)
    if (!route || state.expanded[route.key]) return
    const expanded = _update(state.expanded, {
      [route.key]: { $set: false },
    })
    dispatch({ type: 'setExpanded', expanded })
  }

  function collapse(route: ?RouteOrID) {
    route = getRoute(state, route)
    if (!route || state.expanded[route.key]) return
    const expanded = _update(state.expanded, {
      [route.key]: { $set: true },
    })
    dispatch({ type: 'setExpanded', expanded })
  }

  function toggle(route: ?RouteOrID) {
    route = getRoute(state, route)
    if (!route) return
    console.log('update', state)
    const expanded = _update(state.expanded, {
      [route.key]: { $set: !state.expanded[route.key] },
    })
    console.log('setExpanded', expanded)
    dispatch({ type: 'setExpanded', expanded })
  }

  function add() {
    const { image } = state
    if (!image) return
    const id = getNextId(image)
    const key = `Route${id}`
    const route = {
      id,
      key,
      name: 'New Route',
      items: [],
    }
    const history = _update(image, {
      routes: { $push: [route] },
    })
    dispatch({ type: 'addHistory', history })
  }

  function edit(route: Route) {
    dispatch({ type: 'editRoute', route })
  }

  function update(route: Route, data: Object) {
    const { image } = state
    if (!image) return
    const routeIndex = image.routes.indexOf(route)
    const history = _update(image, {
      routes: {
        [routeIndex]: { $merge: data }
      }
    })
    dispatch({ type: 'addHistory', history })
  }
  
  function remove(route: Route) {
    const { image } = state
    if (!image) return
    const routeIndex = image.routes.indexOf(route)
    const history = _update(image, {
      routes: { $splice: [[routeIndex, 1]] },
    })
    dispatch({ type: 'addHistory', history })
  }

  return {
    set,
    expand,
    collapse,
    toggle,
    add,
    edit,
    update,
    remove,
  }
}

function getNextId({ routes }: Image) {
  if (!routes.length) return 1
  return routes.reduce((id, route) => {
    return route.id >= id ? route.id + 1 : id
  }, 1)
}

function getRoute(state: State, route: ?RouteOrID): ?Route {
  if (typeof route === 'object') {
    return route
  }
  if (typeof route === 'string') {
    route = parseInt(route, 10)
  }
  if (typeof route === 'number' && state.image) {
    return state.image.routes.find(r => r.id === route)
  }
  return null
}
