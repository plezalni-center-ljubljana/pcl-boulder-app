// @flow

export type DrawerActions = {
  open(): void,
  close(): void,
  toggle(): void,
}
