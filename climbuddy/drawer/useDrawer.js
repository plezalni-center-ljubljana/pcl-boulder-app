// @flow

import type { StateArray } from '../context/types'
import type { DrawerActions } from './types'
import { emptyFunction } from '../utils'

export const drawerActions = {
  open: emptyFunction,
  close: emptyFunction,
  toggle: emptyFunction,
}

export function useDrawer([,dispatch]: StateArray): DrawerActions {
  function open() {
    dispatch({ type: 'openDrawer', variant: getVariant() })
  }

  function close() {
    dispatch({ type: 'closeDrawer', variant: getVariant() })
  }

  function toggle() {
    dispatch({ type: 'toggleDrawer', variant: getVariant() })
  }

  return {
    open,
    close,
    toggle,
  }
}

function getVariant() {
  return typeof window !== 'undefined' && window.innerWidth >= 960
    ? 'desktop'
    : 'mobile'
}