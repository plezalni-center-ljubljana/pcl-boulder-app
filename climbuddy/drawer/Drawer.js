// @flow

import React, { useContext } from 'react'

import { makeStyles } from '@material-ui/styles'
import AddIcon from '@material-ui/icons/Add'
import MenuIcon from '@material-ui/icons/Menu'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import { Context } from '../context'
import { Routes } from '../routes'

const useStyles = makeStyles(theme => ({
  root: {},
  paper: {
    width: 300,
    [theme.breakpoints.up('md')]: {
      width: 360,
    },
  },
  toolbar: {
    paddingRight: 6,
  },
  title: {
    flex: 1,
  },
  fab: {
    position: 'absolute',
    margin: 6,
    zIndex: 999,
  },
  icon: {
    margin: 6,
  },
}))

const PaperDrawer = () => {
  const classes = useStyles()
  const {
    actions,
    drawer,
    editing,
    image,
    paper,
  } = useContext(Context)

  if (!image) return null

  const drawerProps = {
    className: classes.root,
    classes: { paper: classes.paper },
    onClose: actions.drawer.close,
  }

  const toolbarProps = {
    className: classes.toolbar,
    disableGutters: true,
  }

  const iconProps = {
    className: classes.icon,
    onClick: actions.drawer.close,
  }

  const toolbar = (
    <Toolbar {...toolbarProps}>
      <IconButton {...iconProps}>
        <MenuIcon />
      </IconButton>
      <Typography className={classes.title} variant={'h6'}>
        {image ? image.name : 'Routes'}
      </Typography>
      {editing &&
        <IconButton className={classes.icon} onClick={actions.route.add}>
          <AddIcon />
        </IconButton>
      }
    </Toolbar>
  )

  return (
    <>
      <IconButton
        className={classes.fab}
        variant={'fab'}
        onClick={actions.drawer.open}
      >
        <MenuIcon />
      </IconButton>
      {/* mobile */}
      <Hidden mdUp implementation={'js'}>
        <Drawer
          {...drawerProps}
          open={drawer.mobile}
          variant={'temporary'}
          ModalProps={{ keepMounted: true }}
        >
          {toolbar}
          <Routes />
        </Drawer>
      </Hidden>
      {/* desktop */}
      <Hidden smDown implementation={'js'}>
        <Drawer
          {...drawerProps}
          open={drawer.desktop}
          variant={'persistent'}
          SlideProps={{
            onEntering: paper.resizeView,
            onExiting: paper.resizeView,
          }}
        >
          {toolbar}
          <Routes />
        </Drawer>
      </Hidden>
    </>
  )
}

export default PaperDrawer
