// @flow

import React from 'react'
import classnames from 'classnames'

import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { makeStyles } from '@material-ui/styles'

import type { Route } from '../route/types'
import type { Item } from '../item/types'

const useStyles = makeStyles(theme => ({
  item: {
    paddingLeft: 40,
  },
  selected: {
    background: theme.palette.grey[200],
  },
}))

type Props = {
  route: Route,
  item: Item,
  selected?: boolean,
}

export default function RouteListItem({ item, selected, ...other }: Props) {
  const classes = useStyles()

  return (
    <ListItem
      {...other}
      key={item.id}
      className={classnames(classes.item, {
        [classes.selected]: selected,
      })}
      component={'li'}
      button
    >
      <ListItemText primary={item.type} />
    </ListItem>
  )
}
