// @flow

import React from 'react'
import type { Node } from 'react'
import Collapse from '@material-ui/core/Collapse'

type Props = {
  children: Node,
  unmountOnExit: boolean,
}

export default function CollapseItems({ children, ...other }: Props) {
  return (
    <Collapse
      {...other}
      component={'li'}
      timeout={'auto'}
      unmountOnExit
    >
      {children}
    </Collapse>
  )
}
