// @flow

import React from 'react'
import classnames from 'classnames'

import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import PenIcon from '@material-ui/icons/Edit'
import IconButton from '@material-ui/core/IconButton'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import { makeStyles } from '@material-ui/styles'

import type { Route } from '../route/types'

const useStyles = makeStyles(theme => ({
  route: {
    minHeight: 56,
    paddingRight: 24,
  },
  routeSelected: {
    background: theme.palette.grey[300],
  },
  actions: {
    paddingRight: 40,
  },
}))

type Props = {
  route: Route,
  expanded?: boolean,
  selected?: boolean,
  onEdit?: Function,
}

export default function RouteItem({
  route,
  expanded,
  selected,
  onEdit,
  ...other
}: Props) {
  const classes = useStyles()

  return (
    <ListItem
      {...other}
      className={classnames(classes.route, {
        [classes.routeSelected]: selected,
      })}
      component={'li'}
      data-route-id={route.id}
      button
    >
      <ListItemText primary={`${route.name} XXXY`} />
      {expanded ? <ExpandLess /> : <ExpandMore />}
      <ListItemSecondaryAction className={classes.actions}>
        {selected &&
          <IconButton onClick={onEdit}>
            <PenIcon />
          </IconButton>
        }
      </ListItemSecondaryAction>
    </ListItem>
  )
}
