// @flow

import React, { Fragment, useContext, useCallback } from 'react'

import List from '@material-ui/core/List'
import { makeStyles } from '@material-ui/styles'

import { Context } from '../context'
import Item from './Item'
import Collapse from './Collapse'
import Route from './Route'

const useStyles = makeStyles({
  list: {},
})

const PaperRoutes = () => {
  const classes = useStyles()

  const {
    actions,
    expanded,
    image,
    paper,
    route: currentRoute,
    selection,
  } = useContext(Context)

  // wait until image is loaded
  if (!image) return null

  const handleRouteClick = useCallback((e) => {
    if (!e) return
    const { routeId } = e.currentTarget.dataset
    const id = parseInt(routeId, 10)
    const route = image.routes.find(r => r.id === id)
    if (route) {
      actions.route.set(route)
      actions.route.toggle(route)
      paper.selectItem(route.key)
    }
  }, [image])
  
  const handleRouteEdit = useCallback((e) => {
    if (!e) return
    console.log('edit')
    const { routeId } = e.currentTarget.dataset
    const id = parseInt(routeId, 10)
    const route = image.routes.find(r => r.id === id)
    if (route) {
      actions.route.edit(route)
    }
  }, [image])
  
  const handleItemClick = useCallback((e) => {
    if (!e) return
    const { selectionId, routeId } = e.currentTarget.dataset
    paper.selectItem(selectionId)
    actions.route.set(routeId)
  }, [image])

  return (
    <List className={classes.list}>
      {image.routes.map(route => (
        <Fragment key={route.id}>
          <Route
            route={route}
            selected={Boolean(currentRoute && route.id === currentRoute.id)}
            expanded={expanded[route.key]}
            onClick={handleRouteClick}
            onEdit={handleRouteEdit}
            data-route-id={route.id}
            button
          />
          <Collapse unmountOnExit in={expanded[route.key]}>
            <List disablePadding>
              {route.items.map(item => (
                <Item
                  key={item.id}
                  item={item}
                  route={route}
                  selected={selection === item.key}
                  onClick={handleItemClick}
                  data-selection-id={item.key}
                  data-route-id={route.id}
                />
              ))}
            </List>
          </Collapse>
        </Fragment>
      ))}
    </List>
  )
}

export default PaperRoutes
