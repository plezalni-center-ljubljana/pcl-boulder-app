// @flow

import { PaperScope } from 'paper/dist/paper-core'

export type Scope = PaperScope

export type PaperActions = {
  exportSvg(): void,
  getScope(): ?Scope,
  resizeView(): void,
  selectItem(item: ?string): void,
  setScope(scope: ?Scope): void,
}
