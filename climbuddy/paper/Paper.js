// @flow

import React, { useContext } from 'react'
import type { Node } from 'react'
import { makeStyles } from '@material-ui/styles'
import { Paper, View } from 'react-paper-renderer'
import { Context } from '../context'

const useStyles = makeStyles({
  root: {
    display: 'block',
    width: '100%',
    height: '100%',
  },
})

type Props = {
  children?: Node,
}

const settings = {
  handleSize: 7,
}

export default function PaperView({ children }: Props) {
  const ctx = useContext(Context)
  const classes = useStyles()

  return (
    <Paper
      className={classes.root}
      settings={settings}
      onScopeReady={ctx.paper.setScope}
    >
      <View
        //onResize={actions.image.reset}
      >
        <Context.Provider value={ctx}>{children}</Context.Provider>
      </View>
    </Paper>
  )
}
