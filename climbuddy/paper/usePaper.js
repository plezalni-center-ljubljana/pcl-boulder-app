// @flow

import { useRef } from 'react'
import { emptyFunction } from '../utils'
import type { StateArray } from '../context/types'
import type { PaperActions, Scope } from './types'

export const paperActions = {
  exportSvg: emptyFunction,
  getScope: emptyFunction,
  resizeView: emptyFunction,
  selectItem: emptyFunction,
  setScope: emptyFunction,
}

export function usePaper([state, dispatch]: StateArray): PaperActions {
  const scopeRef = useRef<?Scope>()

  function getScope() {
    return scopeRef.current || null
  }

  function resizeView() {
    const { current: scope } = scopeRef
    if (scope) {
      const { width, height } = scope.view.element.getBoundingClientRect()
      scope.view.viewSize = new scope.Size(width, height)
    }
  }

  function selectItem(item: ?string) {
    if (item !== state.selection) {
      dispatch({ type: 'setSelection', item })
    }
  }

  function setScope(s: ?Scope) {
    scopeRef.current = s
  }

  function exportSvg() {
    const { current: scope } = scopeRef
    if (scope) {
      const svg = scope.project.exportSVG({
        bounds: 'view',
        asString: false,
        precision: 5,
        matchShapes: false,
        embedImages: true,
      })
      console.log(svg)
    }
  }

  return {
    exportSvg,
    getScope,
    resizeView,
    selectItem,
    setScope,
  }
}
