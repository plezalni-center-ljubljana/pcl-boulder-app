// @flow

import React from 'react'
import {
  CircleTool,
  DeleteTool,
  MoveTool,
  PenTool,
  SelectTool,
} from '../tools'

type Props = {
  editing: boolean,
}

const EditorTools = ({ editing }: Props) => (
  <>
    <MoveTool />
    {editing && (
      <>
        <PenTool />
        <SelectTool />
        <CircleTool />
        <DeleteTool />
      </>
    )}
  </>
)

export default EditorTools
