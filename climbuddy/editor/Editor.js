// @flow

import React, { useEffect } from 'react'

import { Provider, usePaperContext } from '../context'
import type { Image as ImageType } from '../image/types'

import { Dialog } from '../popup'
import { Drawer } from '../drawer'
import { Image } from '../image'
import { Route } from '../route'
import { Paper } from '../paper'
import Layout from './Layout'
import Toolbar from './Toolbar'
import Tools from './Tools'

type Props = {
  image: ImageType,
  editing: boolean,
}

const PaperEditor = ({ image, editing }: Props) => {
  const ctx = usePaperContext({ editing })

  useEffect(() => {
    if (editing !== ctx.editing) {
      ctx.dispatch({ type: 'setEditing', editing })
    }
  }, [editing])

  return (
    <Provider value={ctx}>
      <Drawer />
      <Layout>
        <Toolbar />
        <Paper>
          <Image image={image} />
          {ctx.image && ctx.image.routes.map(route => (
            <Route key={route.id} route={route} />
          ))}
          <Tools editing={editing} />
        </Paper>
        <Dialog />
      </Layout>
    </Provider>
  )
}

export default PaperEditor
