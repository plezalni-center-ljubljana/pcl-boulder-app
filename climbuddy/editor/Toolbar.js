// @flow

import React, { useContext } from 'react'
import Toolbar from '@material-ui/core/Toolbar'
import { makeStyles } from '@material-ui/styles'
import { Context } from '../context'

import {
  CircleButton,
  DeleteButton,
  MoveButton,
  PenButton,
  SelectButton,
  UndoButton,
  RedoButton,
  SaveButton,
} from '../tools'

const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    zIndex: 1,
    top: 0,
    left: 0,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    [theme.breakpoints.down('xs')]: {
      flexWrap: 'wrap',
      justifyContent: 'flex-start',
      margin: 0,
      padding: 0,
      paddingTop: 6,
      paddingRight: 6,
      left: 'auto',
      right: 0,
      width: 56,
      height: '90vh',
      flexDirection: 'column',
      minHeight: '0',
    },
  },
}))

export default function PaperToolbar() {
  const { editing, image } = useContext(Context)
  const classes = useStyles()

  if (!editing || !image) return null

  return (
    <Toolbar className={classes.root}>
      <MoveButton />
      <SelectButton />
      <PenButton />
      <CircleButton />
      <DeleteButton />
      <UndoButton />
      <RedoButton />
      <SaveButton />
    </Toolbar>
  )
}
