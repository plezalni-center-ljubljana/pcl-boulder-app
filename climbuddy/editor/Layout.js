// @flow

import React, { useContext } from 'react'
import type { Node } from 'react'
import classnames from 'classnames'
import { makeStyles } from '@material-ui/styles'
import { Context } from '../context'

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    width: '100%',
    height: '100vh',
  },
  drawer: {
    [theme.breakpoints.up('md')]: {
      width: 'calc(100vw - 360px)',
      marginLeft: 360,
    },
  },
}))

type Props = {
  children: Node,
  className?: string,
  drawer?: boolean,
}

const PaperLayout = ({ children, className, ...other }: Props) => {
  const ctx = useContext(Context)
  const classes = useStyles()

  return (
    <div
      {...other}
      className={classnames(
        classes.root,
        {
          [classes.drawer]: ctx.drawer.desktop,
        },
        className
      )}
    >
      {children}
    </div>
  )
}

export default PaperLayout
