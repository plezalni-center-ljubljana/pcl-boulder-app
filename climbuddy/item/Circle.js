// @flow

import React, { useContext, useRef } from 'react'
import { HitResult } from 'paper/dist/paper-core'
import { Circle } from 'react-paper-renderer'
import { Context } from '../context'

type Props = {
  active: boolean,
  pathData: string,
  selected: boolean,
  strokeColor: string,
  strokeScaling: boolean,
  strokeWidth: number,
}

const PaperCircle = (props: Props) => {
  const { actions } = useContext(Context)
  const hit = useRef<HitResult|null>(null)
  const { active, strokeWidth, ...other } = props

  function handleMouseEnter() {
    if (document.body) {
      document.body.style.cursor = 'pointer'
    }
  }

  function handleMouseLeave() {
    if (document.body) {
      document.body.style.cursor = 'auto'
    }
  }

  function handleMouseDown(e) {
    const circle = e.target

    hit.current = circle.hitTest(e.point, {
      fill: false,
      stroke: true,
      segments: true,
      tolerance: 5,
    })

    console.log(hit.current)
  }
  
  function handleMouseDrag(e) {
    if (!hit.current) return

    const { type } = hit.current

    if (type === 'segment') {
      const circle = e.target
      const center = circle.bounds.center
      const radius = center.getDistance(e.point)
      if (radius < 1) return
      const x = center.x - radius
      const y = center.y - radius
      const size = radius * 2
      circle.fitBounds(x, y, size, size)
    }

    if (type === 'stroke') {
      e.target.translate(e.delta)
    }
  }

  function handleMouseUp(e) {
    if (!hit.current) return

    const circle = e.target

    actions.item.update(e.target, {
      type: 'Circle',
      pathData: circle.pathData,
    })

    hit.current = null
  }

  return (
    <Circle
      {...other}
      name={'Circle'}
      type={'Circle'}
      strokeWidth={active ? strokeWidth + 1 : strokeWidth}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onMouseDown={handleMouseDown}
      onMouseDrag={handleMouseDrag}
      onMouseUp={handleMouseUp}
    />
  )
}

export default PaperCircle
