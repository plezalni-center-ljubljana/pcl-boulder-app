// @flow

import React, { useContext } from 'react'
import { makeStyles } from '@material-ui/styles'
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import { Group, Circle, Raster } from 'react-paper-renderer'
import type { MouseEvent, PointInput, Raster as PaperRaster } from 'paper/dist/paper-core'
import type { Origin } from '../popup/types'
import { Context } from '../context'
import { Popup } from '../popup'
import IconButton from './IconButton'

type Props = {
  active: boolean,
  borderWidth?: number,
  position: PointInput,
  size?: number,
  src: string,
  text: string,
}

type PopupProps = {
  src: string,
  text: string,
}

const useStyles = makeStyles({
  avatar: {
    width: 56,
    height: 56,
    float: 'left',
    marginRight: 16,
  },
})

const AvatarPopup = ({ src, text }: PopupProps) => {
  const classes = useStyles()
  return (
    <Popup title={'Avatar'}>
      <Typography>
        <Avatar
          alt={'Avatar'}
          className={classes.avatar}
          component={'span'}
          src={src}
        />
        {text}
      </Typography>
    </Popup>
  )
}

const PaperAvatar = ({
  src,
  text,
  borderWidth = 2,
  size = 36,
  ...other
}: Props) => {
  const { actions, image } = useContext(Context)

  if (!image) return null

  function handleClick(e: MouseEvent, origin: Origin) {
    actions.popup.open(<AvatarPopup src={src} text={text} />, origin)
  }

  return (
    <IconButton
      {...other}
      name={'Avatar'}
      type={'Avatar'}
      size={size}
      onClick={handleClick}
    >
      {(scaling: number) => (
        <Group name={'Image'} clipped>
          <Circle name={'Clip'} radius={size / 2 - borderWidth * 2} scaling={scaling} />
          <Raster
            name={'Raster'}
            source={src}
            scaling={scaling}
            onLoad={(raster: PaperRaster) => {
              const { width, height } = raster
              const wr = width / size
              const hr = height / size
              const ratio = wr < hr ? wr : hr
              raster.width = width / ratio
              raster.height = height / ratio
            }}
          />
        </Group>
      )}
    </IconButton>
  )
}

export default PaperAvatar
