// @flow

import React, { useContext } from 'react'
import { makeStyles } from '@material-ui/styles'
import { SymbolItem } from 'react-paper-renderer'
import type { MouseEvent, PointInput } from 'paper/dist/paper-core'
import type { Origin } from '../popup/types'
import { Context } from '../context'
import { Popup } from '../popup'
import IconButton from './IconButton'

type Props = {
  active: boolean,
  src: string,
  position: PointInput,
}

type PopupProps = {
  src: string,
}

const useStyles = makeStyles({
  wrapper: {
    position: 'relative',
    maxWidth: '100%',
    width: 560,
    paddingBottom: '56.25%',
  },
  iframe: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
    background: '#eee',
  },
})

const VideoPopup = ({ src }: PopupProps) => {
  const classes = useStyles()
  return (
    <Popup title={'Video'}>
      <div className={classes.wrapper}>
        <iframe
          src={src}
          width={560}
          height={315}
          frameBorder={0}
          allow={
            'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
          }
          allowFullScreen
          className={classes.iframe}
        />
      </div>
    </Popup>
  )
}

const Icon =
  '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M8 5v14l11-7z" fill="#666"/><path d="M0 0h24v24H0z" fill="none"/></svg>'

const PaperVideo = ({ src, ...other }: Props) => {
  const { actions, image } = useContext(Context)

  if (!image) return null

  function handleClick(e: MouseEvent, origin: Origin) {
    actions.popup.open(<VideoPopup src={src} />, origin)
  }

  return (
    <IconButton
      {...other}
      name={'Video'}
      type={'Video'}
      onClick={handleClick}
    >
      {(scaling: number) => (
        <SymbolItem id={'video'} name={'Symbol'} svg={Icon} scaling={scaling * 1.2} />
      )}
    </IconButton>
  )
}

export default PaperVideo
