// @flow

import React, { useContext, useRef } from 'react'
import type { Fiber } from 'react-reconciler'
import { Group, Path, Circle } from 'react-paper-renderer'
import { Context } from '../context'

type Props = {
  active: boolean,
  pathData: string,
  strokeColor: string,
  strokeWidth: number,
}

const PaperPitch = (props: Props) => {
  const { image, zoom } = useContext(Context)
  const pathRef = useRef<Fiber>()

  if (!image) return null

  const { active, strokeWidth, pathData, ...other } = props

  function handleMouseEnter() {
    if (document.body) {
      document.body.style.cursor = 'pointer'
    }
  }

  function handleMouseLeave() {
    if (document.body) {
      document.body.style.cursor = 'auto'
    }
  }

  return (
    <Group
      {...other}
      name={'Pitch'}
      type={'Pitch'}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      {pathRef.current && (
        <Circle
          name={'Circle'}
          radius={8}
          center={pathRef.current.lastSegment.point}
          fillColor={active ? '#fff' : props.strokeColor}
          scaling={1 / zoom}
          strokeScaling={false}
          strokeColor={props.strokeColor}
          strokeWidth={strokeWidth}
        />
      )}
      <Path
        name={'Path'}
        ref={pathRef}
        pathData={pathData}
        strokeScaling={false}
        strokeWidth={active ? strokeWidth + 1 : strokeWidth}
      />
    </Group>
  )
}

export default PaperPitch
