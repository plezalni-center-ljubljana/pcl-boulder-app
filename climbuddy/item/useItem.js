// @flow

import _update from 'immutability-helper'
import { emptyFunction } from '../utils'
import type { StateArray, HistoryItem } from '../context/types'
import type { Image } from '../image/types'
import type { Route } from '../route/types'
import type {
  ItemActions,
  Item,
  AddItem,
  UpdateItem,
  PaperItem,
} from './types'

export const itemActions = {
  add: emptyFunction,
  update: emptyFunction,
  remove: emptyFunction,
}

export function useItem([state, dispatch]: StateArray): ItemActions {

  function add({ type, ...input }: AddItem) {
    const { image, route, routeIndex } = state
    if (!image || !route || !type) return
    const id = getNextId(image)
    const key = `${type}${id}`
    const item: Item = _update(input, { $merge: { id, key, type }})
    const history = addHistory(image, routeIndex, item)
    dispatch({ type: 'addHistory', history })
    dispatch({ type: 'setSelection', item: key })
  }

  function update(paperItem: PaperItem, input: UpdateItem) {
    const { image, route, routeIndex } = state
    if (!image || !route || !paperItem.type) return
    const itemIndex = getItemIndex(route, paperItem)
    const item: Item = _update(route.items[itemIndex], { $merge: input })
    const history = updateHistory(image, routeIndex, itemIndex, item)
    dispatch({ type: 'addHistory', history })
  }

  function remove(paperItem: PaperItem) {
    const { image, route, routeIndex } = state
    if (!image || !route) return
    const itemIndex = getItemIndex(route, paperItem)
    const history = removeHistory(image, routeIndex, itemIndex)
    dispatch({ type: 'addHistory', history })
  }

  return {
    add,
    update,
    remove,
  }
}

function getNextId({ routes }: Image) {
  return routes.reduce(
    (id, route) =>
      route.items.reduce(
        (id, item) => (item.id && item.id >= id) ? item.id + 1 : id,
        id
      ),
    1
  )
}

function getItemIndex({ items }: Route, { data }: PaperItem) {
  return items.findIndex(i => i.id === data.itemId)
}

function addHistory(
  image: Image,
  routeIndex: number,
  item: Item,
): HistoryItem {
  return _update(image, {
    routes: {
      [routeIndex]: {
        items: { $push: [item] },
      },
    },
  })
}

function updateHistory(
  image: Image,
  routeIndex: number,
  itemIndex: number,
  updatedItem: Item,
): HistoryItem {
  return _update(image, {
    routes: {
      [routeIndex]: {
        items: {
          [itemIndex]: {
            $set: updatedItem,
          },
        },
      },
    },
  })
}

function removeHistory(
  image: Image,
  routeIndex: number,
  itemIndex: number,
): HistoryItem {
  return _update(image, {
    routes: {
      [routeIndex]: {
        items: {
          $splice: [[itemIndex, 1]],
        },
      },
    },
  })
}