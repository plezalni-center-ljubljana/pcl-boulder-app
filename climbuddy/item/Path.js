// @flow

import React from 'react'
import { Path } from 'react-paper-renderer'

type Props = {
  active: boolean,
  pathData: string,
  selected: boolean,
  strokeColor: string,
  strokeScaling: boolean,
  strokeWidth: number,
}

const PaperPath = (props: Props) => {
  const { active, strokeWidth, ...other } = props

  function handleMouseEnter() {
    if (document.body) {
      document.body.style.cursor = 'pointer'
    }
  }

  function handleMouseLeave() {
    if (document.body) {
      document.body.style.cursor = 'auto'
    }
  }

  return (
    <Path
      {...other}
      name={'Path'}
      type={'Path'}
      strokeWidth={active ? strokeWidth + 2 : strokeWidth}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    />
  )
}

export default PaperPath
