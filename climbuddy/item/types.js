// @flow

import type { PointInput, Item as BaseItem } from 'paper/dist/paper-core'

export type ItemActions = {
  add(input: AddItem): void,
  update(item: PaperItem, input: UpdateItem): void,
  remove(item: PaperItem): void,
}

export type PaperItem = BaseItem & {
  key?: string,
  type?: ItemType,
  comment?: string,
  icon?: string,
  src?: string,
  text?: string,
}

export type Item = {
  id: number,
  key: string,
  type: ItemType,
  comment?: string,
  icon?: string,
  src?: string,
  text?: string,
  position?: PointInput,
  pathData?: string,
  strokeColor?: string,
  strokeScaling?: boolean,
  strokeWidth?: number,
}

export type AddItem = {
  id?: number,
  key?: string,
  type: ItemType,
  comment?: string,
  icon?: string,
  src?: string,
  text?: string,
  position?: PointInput,
  pathData?: string,
  strokeColor?: string,
  strokeScaling?: boolean,
  strokeWidth?: number,
}

export type UpdateItem = {
  id?: number,
  key?: string,
  type?: ItemType,
  comment?: string,
  icon?: string,
  src?: string,
  text?: string,
  position?: PointInput,
  pathData?: string,
  strokeColor?: string,
  strokeScaling?: boolean,
  strokeWidth?: number,
}

export type ItemType =
  'Avatar' |
  'Circle' |
  'Comment' |
  'Icon' |
  'Path' |
  'Pitch' |
  'Video'

export type AvatarItem = {
  id: number,
  key: string,
  type: 'Avatar',
  src: string,
  text: string,
  position: PointInput,
}

export type CircleItem = {
  id: number,
  key: string,
  type: 'Circle',
  pathData: string,
  strokeColor: string,
  strokeScaling: boolean,
  strokeWidth: number,
}

export type CommentItem = {
  id: number,
  key: string,
  type: 'Comment',
  comment: string,
  position: PointInput,
}

export type IconItem = {
  id: number,
  key: string,
  type: 'Icon',
  icon: string,
  position: PointInput,
}

export type PathItem = {
  id: number,
  key: string,
  type: 'Path',
  pathData: string,
  strokeColor: string,
  strokeScaling: boolean,
  strokeWidth: number,
}

export type PitchItem = {
  id: number,
  key: string,
  type: 'Pitch',
  pathData: string,
  strokeColor: string,
  strokeScaling: boolean,
  strokeWidth: number,
}

export type VideoItem = {
  id: number,
  key: string,
  type: 'Video',
  src: string,
  position: PointInput,
}

