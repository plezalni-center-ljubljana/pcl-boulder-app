// @flow

import React, { useContext, /*useEffect,*/ useRef } from 'react'
import type { PointInput } from 'paper/dist/paper-core'
import { SymbolItem } from 'react-paper-renderer'
import { Context } from '../context'
import IconButton from './IconButton'

type Props = {
  active: boolean,
  icon: string,
  position: PointInput,
}

const Icons = {
  piton:
    '<svg version="1.1" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><path d="m3.1777 27.92 23.778-23.778" stroke="#000" stroke-width="3"/><path d="m26.464 5.7513a7.0269 7.0269 0 0 1 1.9849 6.8634 7.0269 7.0269 0 0 1-5.0504 5.0536 7.0269 7.0269 0 0 1-6.8646-1.9806" fill="none" stroke="#000" stroke-width="3"/></svg>',
}

const PaperIcon = ({ icon, ...props }: Props) => {
  const { image } = useContext(Context)
  const iconRef = useRef()

  /*
  useEffect(() => {
    if (props.active && iconRef.current) {
      const icon = iconRef.current
      icon.layer.children.forEach(child => {
        if (child !== icon && icon.intersects(child)) {
          //console.log('intersects', child)
        }
      })
    }
  })
  */

  if (!image) return null

  return (
    <IconButton
      {...props}
      name={'Icon'}
      type={'Icon'}
      ref={iconRef}
      size={30}
    >
      {(scaling: number) => (
        <SymbolItem
          id={icon}
          name={'Symbol'}
          svg={Icons[icon]}
          scaling={scaling * 0.6}
          strokeScaling={false}
          strokeWidth={2}
        />
      )}
    </IconButton>
  )
}

export default PaperIcon
