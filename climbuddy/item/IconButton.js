// @flow

import React, { useContext, useEffect, useState, forwardRef } from 'react'
import { Group, Circle } from 'react-paper-renderer'
import { Motion, spring } from 'react-motion'
import type { PointInput, MouseEvent } from 'paper/dist/paper-core'
import { Context } from '../context'

type Props = {
  active: boolean,
  children: Function,
  name?: string,
  position: PointInput,
  size?: number,
  selected?: boolean,
  onClick?: Function,
}

const IconButton = forwardRef<*,*>(({ active, ...props }: Props, ref: Function) => {
  const { activeTool, zoom } = useContext(Context)
  const [hover, setHover] = useState(false)
  const [show, setShow] = useState(false)

  useEffect(() => setShow(active), [active])

  const { children, size = 36, selected, onClick, ...other } = props
  const enabled = activeTool === 'move'

  function handleMouseEnter() {
    if (document.body) {
      document.body.style.cursor = 'pointer'
    }
    setHover(true)
  }

  function handleMouseLeave() {
    if (document.body) {
      document.body.style.cursor = 'auto'
    }
    setHover(false)
  }

  function handleClick(e: MouseEvent) {
    setHover(false)
    if (!onClick) return
    const { point, target } = e
    const { view } = target
    const { x, y } = view.projectToView(point)
    const { top, left } = view.element.getBoundingClientRect()
    onClick(e, { x: x + left, y: y + top })
  }

  const handlers = {
    onMouseEnter: enabled ? handleMouseEnter : null,
    onMouseLeave: enabled ? handleMouseLeave : null,
    onClick: enabled && onClick ? handleClick : null,
  }

  return (
    <Motion style={{ scale: spring(hover ? 1.4 : show ? 1 : 0.001) }}>
      {({ scale }) => (
        <Group
          name={'IconButton'}
          {...other}
          {...handlers}
          ref={ref}
        >
          <Circle
            name={'Circle'}
            fillColor={'#fff'}
            radius={size / 2}
            selected={selected}
            scaling={(1 / zoom) * scale}
          />
          {children((1 / zoom) * scale)}
        </Group>
      )}
    </Motion>
  )
})

IconButton.displayName = 'IconButton'

export default IconButton
