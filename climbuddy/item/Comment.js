// @flow

import React, { useContext } from 'react'
import Typography from '@material-ui/core/Typography'
import { SymbolItem } from 'react-paper-renderer'
import type { MouseEvent, PointInput } from 'paper/dist/paper-core'
import type { Origin } from '../popup/types'
import { Context } from '../context'
import { Popup } from '../popup'
import IconButton from './IconButton'

type Props = {
  active: boolean,
  comment: string,
  size?: number,
  position: PointInput,
}

type PopupProps = {
  comment: string,
}

const CommentPopup = ({ comment }: PopupProps) => {
  return (
    <Popup title={'Comment'}>
      <Typography>{comment}</Typography>
    </Popup>
  )
}

const Icon =
  '<svg version="1.1" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><path d="M16 2c8.837 0 16 5.82 16 13s-7.163 13-16 13c-0.849 0-1.682-0.054-2.495-0.158-3.437 3.437-7.539 4.053-11.505 4.144v-0.841c2.142-1.049 4-2.961 4-5.145 0-0.305-0.024-0.604-0.068-0.897-3.619-2.383-5.932-6.024-5.932-10.103 0-7.18 7.163-13 16-13z" fill="#666" /></svg>'

const PaperComment = ({ comment, ...other }: Props) => {
  const { actions, image } = useContext(Context)

  if (!image) return null

  function handleClick(e: MouseEvent, origin: Origin) {
    actions.popup.open(<CommentPopup comment={comment} />, origin)
  }

  return (
    <IconButton
      {...other}
      name={'Comment'}
      type={'Comment'}
      onClick={handleClick}
    >
      {(scaling: number) => (
        <SymbolItem id={'comment'} svg={Icon} scaling={scaling * 0.55} />
      )}
    </IconButton>
  )
}

export default PaperComment
