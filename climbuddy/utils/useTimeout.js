// @flow

import { useEffect, useRef } from 'react'

type TimeoutRef = { current: TimeoutID | null }

export function useTimeout() {
  const timeout: TimeoutRef = useRef(null)

  useEffect(
    () => () => {
      if (timeout.current) {
        clearTimeout(timeout.current)
      }
    },
    []
  )

  function setTimeout(fn: Function, wait: number) {
    if (timeout.current) {
      clearTimeout(timeout.current)
    }
    timeout.current = window.setTimeout(() => {
      fn()
      timeout.current = null
    }, wait)
  }

  return setTimeout
}
