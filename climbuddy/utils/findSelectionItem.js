// @flow

import type { PaperItem } from '../item/types'

export function findSelectionItem(item: PaperItem): PaperItem | null {
  if (item.data.selectionId) {
    return item
  }
  if (item.parent) {
    return findSelectionItem(item.parent)
  }
  return null
}
