// @flow

const dblTapMaxDelay = 300
let latestTouchTap = {
  time: 0,
  target: null,
}

export function doubleTap(e: MouseEvent | TouchEvent) {
  const touchTap = {
    time: new Date().getTime(),
    target: e.currentTarget,
  }
  const isFastDblTap =
    touchTap.target === latestTouchTap.target &&
    touchTap.time - latestTouchTap.time < dblTapMaxDelay
  latestTouchTap = touchTap
  return isFastDblTap
}
