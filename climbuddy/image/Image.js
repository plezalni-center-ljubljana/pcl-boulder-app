// @flow

import React, { useContext, useEffect } from 'react'
import { Layer, Raster } from 'react-paper-renderer'
import { Raster as PaperRaster } from 'paper/dist/paper-core'
import { Context } from '../context'
import type { Image } from './types'

type Props = {
  image: Image,
}

export default function PaperImage({ image }: Props) {
  const { actions, image: currentImage } = useContext(Context)

  function handleImageLoad(raster: PaperRaster) {
    if (raster && raster.view) {
      raster.fitBounds(0, 0, image.width, image.height)
      actions.image.set(image)
    }
  }

  useEffect(() => () => actions.image.set(null), [])

  return (
    <Layer name={'image'}>
      <Raster
        locked
        source={image.src}
        onLoad={handleImageLoad}
        visible={!!currentImage}
      />
    </Layer>
  )
}
