// @flow

export const sampleImage = {
  id: 1,
  key: 'Image11',
  name: 'Image 1',
  src: '/static/mr-bubbles-1080.jpg',
  width: 1920,
  height: 870,
  routes: [{
    id: 1,
    key: 'Route1',
    type: 'Route',
    name: 'Route 1',
    items: [{
      id: 1,
      key: 'Path1',
      type: 'Path',
      strokeColor: '#d500f9',
      strokeScaling: false,
      strokeWidth: 2,
      pathData: 'M177.02128,699.17021c14.25138,-14.25138 31.86427,-36.4945 40.85106,-54.46809c3.12112,-6.24223 0.90261,-39.93287 2.7234,-43.57447c6.67805,-13.3561 6.94469,-30.2298 13.61702,-43.57447c19.24589,-38.49177 46.93214,-79.61299 76.25532,-108.93617c0.9078,-0.9078 1.8156,-7.26241 2.7234,-8.17021c5.44352,-5.44352 15.59321,-15.59321 21.78723,-21.78723c0.9078,-0.9078 1.8156,-7.26241 2.7234,-8.17021c5.25033,-5.25033 19.26031,-8.36669 24.51064,-13.61702c8.71923,-8.71923 36.0829,-28.93507 49.02128,-35.40426c2.17872,-1.08936 5.99149,1.08936 8.17021,0c11.23195,-5.61598 18.72549,-18.89466 29.95745,-24.51064c5.37154,-2.68577 10.83304,-5.41652 16.34043,-8.17021c1.64432,-0.82216 6.91128,1.25893 8.17021,0c4.99894,-4.99894 14.04119,-9.744 21.78723,-13.61702c11.08241,-5.5412 24.60593,-21.83488 35.40426,-27.23404c1.65055,-0.82527 7.02899,-1.58218 8.17021,-2.7234c6.44051,-6.44051 19.68055,-22.09559 29.95745,-27.23404c2.47043,-1.23521 9.11605,-3.66924 10.89362,-5.44681c4.5806,-4.5806 9.0661,-15.42667 16.34043,-19.06383c13.35675,-6.67838 26.62094,-21.48068 38.12766,-27.23404c2.80818,-1.40409 20.90203,-7.28501 21.78723,-8.17021c21.266,-21.266 40.61421,-35.1674 59.91489,-54.46809c7.55898,-7.55898 22.41113,-30.26939 32.68085,-35.40426c14.6744,-7.3372 26.80547,-18.63526 38.12766,-29.95745c14.0874,-14.0874 26.92826,-23.89908 35.40426,-40.85106c1.86848,-3.73695 7.1589,-13.61702 10.89362,-13.61702',
    },{
      id: 2,
      key: 'Circle2',
      type: 'Circle',
      pathData: 'M140.02128,693.7234c0,-20.43454 16.56546,-37 37,-37c20.43454,0 37,16.56546 37,37c0,20.43454 -16.56546,37 -37,37c-20.43454,0 -37,-16.56546 -37,-37z',
      strokeColor: '#d500f9',
      strokeWidth: 2,
      strokeScaling: false,
    },{
      id: 3,
      key: 'Comment3',
      type: 'Comment',
      position: [300,600],
      comment: 'Test comment :) Sure hope it works ... \n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },{
      id: 4,
      key: 'Avatar4',
      type: 'Avatar',
      position: [200,400],
      src: '/static/avatar.jpg',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    },{
      id: 5,
      key: 'Video5',
      type: 'Video',
      position: [600,600],
      src: 'https://www.youtube.com/embed/N6mb-VlY22o',
    }]
  },{
    id: 2,
    key: 'Route2',
    type: 'Route',
    name: 'Route 2',
    items: [{
      id: 6,
      key: 'Path6',
      type: 'Path',
      strokeColor: '#ffea00',
      strokeScaling: false,
      strokeWidth: 2,
      pathData: 'M1481.53191,671.93617c0,-15.22643 -15.38771,-33.49883 -21.78723,-46.29787c-3.68828,-7.37656 -1.62558,-15.2426 -8.17021,-21.78723c-1.3617,-1.3617 1.3617,-6.80851 0,-8.17021c-3.18275,-3.18275 -10.17312,-7.44972 -13.61702,-10.89362c-3.61158,-3.61158 -5.90581,-11.81162 -8.17021,-16.34043c-3.4958,-6.9916 -12.27251,-14.99591 -16.34043,-19.06383c-1.28383,-1.28383 1.28383,-4.16298 0,-5.44681c-6.70708,-6.70708 -14.18438,-19.63118 -19.06383,-24.51064c-3.75349,-3.75349 -5.72983,-14.18307 -8.17021,-19.06383c-3.25972,-6.51943 -32.28249,-40.65189 -38.12766,-43.57447c-11.66067,-5.83034 -37.29955,3.67105 -46.29787,8.17021c-6.78981,3.3949 -17.81671,-0.62356 -24.51064,2.7234c-11.72649,5.86325 -60.70265,10.89362 -76.25532,10.89362c-7.92044,0 -25.75036,3.46524 -32.68085,0c-5.11088,-2.55544 -3.91631,-8.76667 -8.17021,-10.89362c-8.59932,-4.29966 -24.9638,-22.69355 -29.95745,-32.68085c-1.62244,-3.24488 -2.98742,-11.15763 -5.44681,-13.61702c-4.76092,-4.76092 -15.94967,-15.94967 -21.78723,-21.78723c-2.4151,-2.4151 -1.08214,-7.61108 -2.7234,-10.89362c-0.85319,-1.70638 -9.86975,-9.86975 -10.89362,-10.89362c-2.4277,-2.4277 -1.27724,-8.00129 -2.7234,-10.89362c-0.78331,-1.56662 -19.24945,-20.51834 -21.78723,-21.78723c-5.80993,-2.90496 -15.9773,2.90496 -21.78723,0c-9.44213,-4.72106 -60.26317,-5.27267 -70.80851,0c-2.85723,1.42862 -11.44331,-2.17372 -13.61702,0c-0.64191,0.64191 0.81196,2.31742 0,2.7234c-6.53617,3.26809 -17.97447,-3.26809 -24.51064,0c-4.90906,2.45453 -37.91212,5.55458 -43.57447,2.7234c-11.8988,-5.9494 -20.06284,-31.81866 -27.23404,-35.40426c-5.00704,-2.50352 -14.59806,-10.13229 -19.06383,-19.06383c-0.82527,-1.65055 -1.58218,-7.02899 -2.7234,-8.17021c-6.26156,-6.26156 -11.64263,-14.36604 -16.34043,-19.06383c-1.20622,-1.20622 -6.26576,-20.70173 -8.17021,-24.51064c-1.8156,-3.63121 1.8156,-9.98582 0,-13.61702c-4.40905,-8.8181 -16.34043,-71.21626 -16.34043,-81.70213c0,-15.83637 1.94661,-40.07426 10.89362,-49.02128c1.9013,-1.9013 -0.05808,-19.00575 5.44681,-24.51064c0.64191,-0.64191 0,-1.8156 0,-2.7234c0,-10.39875 3.95772,-21.53247 8.17021,-29.95745c0.81142,-1.62284 2.7234,-15.2005 2.7234,-8.17021',
    },{
      id: 7,
      key: 'Circle7',
      type: 'Circle',
      strokeColor: '#ffea00',
      strokeWidth: 2,
      strokeScaling: false,
      pathData: 'M1440.3617,674.65957c0,-20.43454 16.56546,-37 37,-37c20.43454,0 37,16.56546 37,37c0,20.43454 -16.56546,37 -37,37c-20.43454,0 -37,-16.56546 -37,-37z',
    }]
  },{
    id: 3,
    key: 'Route3',
    type: 'Route',
    name: 'Multipitch',
    items: [
      {
        id: 8,
        key: 'Pitch8',
        type: 'Pitch',
        pathData: 'M917.04958,313.82104c5.67409,-5.67409 8.16382,-14.006 11.60822,-20.8948c6.74947,-13.49893 24.27542,-28.91871 34.82467,-39.46796c7.97879,-7.97879 23.74635,-12.13813 30.18138,-18.57316c2.44222,-2.44222 19.81274,0 23.21644,0c15.67521,0 32.52141,-0.01838 41.7896,-9.28658c1.09443,-1.09443 3.54886,1.09443 4.64329,0c7.21955,-7.21955 7.07562,-21.11618 11.60822,-30.18138c1.39299,-2.78597 5.57195,-4.17896 6.96493,-6.96493c5.50486,-11.00973 8.52019,-24.00532 13.92987,-34.82467c4.34568,-8.69136 0.03775,-23.2542 6.96493,-30.18138',
        strokeColor: '#91ff35',
        strokeWidth: 2,
        strokeScaling: false,
      },
      {
        id: 9,
        key: 'Pitch9',
        type: 'Pitch',
        pathData: 'M935.62273,494.90931c0,-32.99815 5.70442,-65.40483 -2.32164,-97.50907c-0.89663,-3.58652 1.16477,-8.1139 0,-11.60822c-2.71311,-8.13933 -4.73506,-16.61858 -6.96493,-25.53809c-1.77778,-7.11114 3.23371,-19.07067 0,-25.53809c-2.64512,-5.29024 -9.28658,-12.61646 -9.28658,-18.57316',
        strokeColor: '#91ff35',
        strokeWidth: 2,
        strokeScaling: false,
      },
      {
        id: 10,
        key: 'Pitch10',
        type: 'Pitch',
        pathData: 'M812.57557,645.8162c9.67938,-9.67938 19.10273,-21.31846 30.18138,-30.18138c13.55263,-10.8421 32.42771,-18.49784 44.11125,-30.18138c2.05882,-2.05882 0.9225,-6.48829 2.32164,-9.28658c2.89608,-5.79215 8.77709,-10.58925 11.60822,-16.25151c0.92866,-1.85732 -0.92866,-5.10762 0,-6.96493c8.59859,-17.19719 21.0969,-41.9917 34.82467,-55.71947',
        strokeColor: '#91ff35',
        strokeWidth: 2,
        strokeScaling: false,
      },
      {
        id: 11,
        key: 'Pitch11',
        type: 'Pitch',
        pathData: 'M735.96131,785.11487c0,-12.00854 11.86044,-21.52536 16.25151,-32.50302c2.53321,-6.33302 3.10314,-14.13666 6.96493,-20.8948c11.0369,-19.31457 24.35629,-34.78272 34.82467,-55.71947c5.45198,-10.90396 18.57316,-19.12333 18.57316,-30.18138',
        strokeColor: '#91ff35',
        strokeWidth: 2,
        strokeScaling: false,
      },
      {
        id: 12,
        key: 'Icon12',
        type: 'Icon',
        icon: 'piton',
        position: [770,610],
      },
      {
        id: 13,
        key: 'Icon13',
        type: 'Icon',
        icon: 'piton',
        position: [885,470],
      },
      {
        id: 14,
        key: 'Icon14',
        type: 'Icon',
        icon: 'piton',
        position: [970,310],
      }
    ],
  }],
}
