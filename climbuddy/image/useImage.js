// @flow

import { View } from 'paper/dist/paper-core'
import { emptyFunction } from '../utils'
import type { StateArray } from '../context/types'
import type { PaperActions } from '../paper/types'
import type { Image, ImageActions } from './types'

export const imageActions = {
  reset: emptyFunction,
  set: emptyFunction,
  zoom: emptyFunction,
}

export const useImage = (
  [state, dispatch]: StateArray,
  paper: PaperActions
): ImageActions => {
  function set(image: ?Image) {
    const scope = paper.getScope()
    dispatch({ type: 'setImage', image })
    if (scope && image) {
      fitImage(scope.view, image)
      zoom(scope.view.zoom)
    }
  }

  function reset() {
    const scope = paper.getScope()
    if (scope && state.image) {
      fitImage(scope.view, state.image)
      zoom(scope.view.zoom)
    }
  }

  function zoom(zoom: number) {
    dispatch({ type: 'setZoom', zoom })
  }

  return {
    reset,
    set,
    zoom,
  }
}

function fitImage(view: View, image: Image) {
  const {
    viewSize: { width, height },
  } = view

  const wr = width / image.width
  const hr = height / image.height

  const zoom = wr < hr ? wr : hr

  const iw = image.width * zoom
  const ih = image.height * zoom

  const x = (width - iw) / 2 / zoom
  const y = (height - ih) / 2 / zoom

  view.scale(zoom / view.zoom)
  view.translate(
    x - view.matrix.tx / view.zoom,
    y - view.matrix.ty / view.zoom
  )
}
