// @flow

import { Raster } from 'paper/dist/paper-core'
import type { Route } from '../route/types'

export type Image = {
  id: number,
  key: string,
  name: string,
  src: string,
  width: number,
  height: number,
  routes: Array<Route>,
}

export type ImageActions = {
  reset(): void,
  set(image: ?Image, raster?: Raster): void,
  zoom(zoom: number): void,
}
