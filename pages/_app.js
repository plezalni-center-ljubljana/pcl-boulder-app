// @flow

import React from 'react'
import type { Node } from 'react'
import NextApp from 'next/app'
import Head from 'next/head'

import { ThemeProvider } from '@material-ui/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

import theme from 'theme'

type Props = {
  Component: Node,
  pageProps: Object,
}

export default class App extends NextApp<Props> {

  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles)
    }
  }

  componentDidCatch(e: Object) {
    console.log('[app] exception', e)
  }

  render() {
    const { Component, pageProps } = this.props

    return (
      <>
        <Head>
          <title>Boulder App</title>
        </Head>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Component pageContext={this.pageContext} {...pageProps} />
        </ThemeProvider>
      </>
    )
  }
}
