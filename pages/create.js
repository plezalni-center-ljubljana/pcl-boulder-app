// @flow

import React from 'react'
import { Editor } from 'editor'

const Create = () => {
  return (
    <Editor image={'/static/mr-bubbles-1080.jpg'} />
  )
}

export default Create
