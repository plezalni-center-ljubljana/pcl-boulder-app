// @flow

import React, { useState } from 'react'
import { makeStyles } from '@material-ui/styles'
import Button from '@material-ui/core/Button'
import Toolbar from '@material-ui/core/Toolbar'

import { Editor, sampleImage } from 'climbuddy'

const useStyles = makeStyles({
  root: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    paddingLeft: 16,
    zIndex: 1299,
  },
})

const Index = () => {
  const [visible, setVisible] = useState(true)
  const [editing, setEditing] = useState(true)
  const classes = useStyles()

  return (
    <>
      <Toolbar classes={classes}>
        <Button onClick={() => setVisible(!visible)}>
          {visible ? 'Hide' : 'Show'}
        </Button>
        <Button onClick={() => setEditing(!editing)}>
          {editing ? 'Stop Editing' : 'Start Editing'}
        </Button>
      </Toolbar>
      {visible &&
        <Editor image={sampleImage} editing={editing} />
      }
    </>
  )
}

export default Index
