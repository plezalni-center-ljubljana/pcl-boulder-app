// @flow

import React, { useState } from 'react'
import { Motion, spring } from 'react-motion'

import {
  Paper,
  View,
  Group,
  Circle,
  PointText,
  Rectangle,
} from 'react-paper-renderer'

const styles = {
  view: {
    width: '100%',
    height: '100vh',
    margin: 0,
    padding: 0,
    display: 'block'
  },
}

const Index = () => {
  const [open, setOpen] = useState(false)
  const [rotation, setRotation] = useState(0)
  const [hue, setHue] = useState(0)

  function handleFrame() {
    setRotation(rotation + 1)
    setHue(hue === 360 ? 0 : hue + 1)
  }

  function toggleOpen() {
    setOpen(!open)
  }

  return (
    <Paper style={styles.view}>
      <View onFrame={open ? null : handleFrame}>
        <Group rotation={rotation} >
          <Motion style={{size: spring(open ? 400 : 120)}}>
            {({size}) =>
              <>
                <Rectangle
                  center={[300,300]}
                  size={[size,size/2]}
                  opacity={0.8}
                  fillColor={{ hue, saturation: 1, brightness: 1 }}
                  onClick={toggleOpen}
                />
                
              </>
            }
          </Motion>
        </Group>
        <Motion style={{radius: spring(open ? 100 : 40)}}>
          {({radius}) =>
            <Circle
              center={[600, 400]}
              radius={radius}
              fillColor={'green'}
              onClick={toggleOpen}
            />
          }
        </Motion>
        <Motion style={{fontSize: spring(open ? 44 : 22)}}>
          {({fontSize}) =>
            <PointText
              point={[258,507]}
              content={'La bombaa!'}
              fillColor={'#000'}
              fontSize={fontSize}
              onClick={toggleOpen}
            />
          }
        </Motion>
      </View>
    </Paper>
  )
}

export default Index
