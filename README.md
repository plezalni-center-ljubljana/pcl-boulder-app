# Boulder Ionic React

## Features
- Login
- Boulder list
- View boulder
- Add boulder
- Edit boulder
- Delete boulder

## Schema

User
- username
- password
- email

Folder
- user*
- parent*
- name

Route
- user*
- folder*
- name
- image
- layers?

Layer
- route*
- data