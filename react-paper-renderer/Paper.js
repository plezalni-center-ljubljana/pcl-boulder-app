// @flow

import React, { useEffect, useRef } from 'react'
import type { FiberRoot } from 'react-reconciler'
import { PaperScope } from 'paper/dist/paper-core'
import Renderer from './Renderer'

type Props = {
  children: React$Node,
  settings?: Object,
  onScopeReady?: Function,
}

export default function Paper(props: Props) {
  const canvasRef = useRef<HTMLCanvasElement|null>(null)
  const scopeRef = useRef<PaperScope|null>(null)
  const rootRef = useRef<FiberRoot|null>(null)

  const {
    children,
    onScopeReady,
    settings,
    ...other
  } = props

  useEffect(() => {
    const canvas = canvasRef.current

    if (canvas instanceof HTMLCanvasElement) {
      const scope = new PaperScope()

      if (typeof settings === 'object') {
        Object.assign(scope.settings, settings)
      }
      
      scopeRef.current = scope
      scope.setup(canvas)

      const root = Renderer.createContainer(scope)
      rootRef.current = root

      if (typeof onScopeReady === 'function') {
        onScopeReady(scope)
      }
    }
    return () => {
      Renderer.updateContainer(null, rootRef.current)
      canvasRef.current = null
      scopeRef.current = null
      rootRef.current = null
    }
  }, [])

  useEffect(() => {
    if (scopeRef.current && rootRef.current) {
      Renderer.updateContainer(children, rootRef.current)
    }
  }, [children])

  return <canvas {...other} ref={canvasRef} resize={'true'} />
}
