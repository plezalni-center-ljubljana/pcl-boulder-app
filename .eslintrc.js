module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
  ],
  plugins: [
    'flowtype',
  ],
  parser: 'babel-eslint',
  settings: {
    react: {
      createClass: 'createReactClass',
      pragma: 'React',
      version: '16.8.6',
      flowVersion: '0.102.0',
    },
  },
  env: {
    browser: true,
    node: true,
    jest: true,
    es6: true,
  },
  rules: {
    'no-console': 0,
    'flowtype/define-flow-type': 1,
    'flowtype/use-flow-type': 1,
  },
}